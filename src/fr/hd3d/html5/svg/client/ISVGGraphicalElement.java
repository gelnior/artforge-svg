package fr.hd3d.html5.svg.client;



public interface ISVGGraphicalElement extends ISVGConditionalProcessingElement, ISVGStylable, ISVGTransformable
{}
