package fr.hd3d.html5.svg.client.dom.constants;

public enum ZoomAndPanValue
{
    UNKNOWN, DISABLE, MAGNIFY;
}
