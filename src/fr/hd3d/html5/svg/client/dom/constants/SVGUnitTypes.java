package fr.hd3d.html5.svg.client.dom.constants;

public enum SVGUnitTypes
{
    UNKNOWN, USERSPACEONUSE, OBJECTBOUNDINGBOX;
}
